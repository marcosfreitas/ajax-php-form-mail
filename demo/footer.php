

      <script src="<?php echo $abs; ?>loja/wp-content/themes/organic_shop/assets/bootstrap/js/bootstrap.min.js?ver=3.6"></script>
      <script type="text/javascript" src="<?php echo $abs; ?>js/colorbox/jquery.colorbox-min.js"></script>
      <script type="text/javascript" src="<?php echo $abs; ?>js/colorbox/i18n/jquery.colorbox-pt-br.js"></script>



<script>
(function($){
    $get = "<?php echo $_SERVER['QUERY_STRING']?>";
    if($get !== ""){
      if($('.nav').find('.'+$get).addClass('active')){
        $('.nav li').eq(0).removeClass('active');
      }
    }

    $('.light-album').click(function(){
       rel = $(this).attr('rel');
       $('.'+rel).colorbox({
            rel: rel,
            slideshow: true,
            transition: 'fade',
            speed: 800,
            slideshowSpeed: 7000,
            fixed: true,
            preLoading: true,
            previous: "Anterior",
            next: "Próxima",
            current: "Imagem {current} de {total}",
            maxWidth:'95%', maxHeight:'95%'
       });
    });
    //window.addEventListener("orientationchange", function(){$.colorbox.resize({width:'90%'});}, false);
    //window.addEventListener("resize", function(){$.colorbox.resize({width:'90%'});}, false);

     $.vegas({
         src:'<?php echo $abs; ?>loja/wp-content/uploads/2013/12/wedding-marriage-pink-black.jpg',
	       fade:300 // milliseconds
     });
     $('.slide').carousel({
          interval: 5000
     });

    $('#msg_felicitacoes').click(function(){
        $('#mensagem').modal()
    });
    $('#msg_contato').click(function(){
        $('#contato').modal()
    });

    /*envia formulário*/

    // função para enviar o formulário

    $.fn.enviaForm = function enviaForm(formulario, alvo){

       //variáveis
        mensagem   = null;
        formulario = '#'+formulario;
        campos     = $(formulario).serialize();
        modal      = $(formulario).parent().closest('.modal').attr('id');
        modal      = '#'+modal;
        //console.warn(campos);
            $.ajax({

              url           : alvo,
              type          : 'GET',
              cache         : false,
              dataType      : 'json',
              //contentType : "application/x-www-form-urlencoded;charset=ISO-8859-1",
              contentType   : "application/json;charset=utf-8",
              data          : campos,

              success: function(json, textStatus, xhr) {

                //volta botao e preloader ao normal
                
                if(json.sys === '007'){

                //reseta botão
                $(modal).find('button[submit]').button('reset');

                //mensagem final
                $(modal).find('.area51-text').html('Mensagem enviada com sucesso, obrigado! :)');

                $(modal).find('.alert').addClass('in').removeClass('out').show();

                }else{

                    mensagem = mensagem !== null ? mensagem + '<p>'+json.msg+'</p>' : json.msg ;
                    //aviso geral
                    $(modal).find('.area51-text').html(mensagem);

                    $(modal).find('.alert').addClass('in').removeClass('out').show();

                    $(modal).find('button[submit]').button('reset');

                 }
            },

              error: function(xhr, textStatus, errorThrown) {
                console.log(xhr +' '+ textStatus+' '+ errorThrown);
                    //aviso geral
                    $(modal).find('.area51-text').html('<strong>Tente novamente. Se persistir o erro entre em contato com o suporte.</strong>');

                    $(modal).find('.alert').addClass('in').removeClass('out').show();

                    $(modal).find('button[submit]').button('reset');
              }

            });
       return this;

    };


    /*---------------*/
    $('form button').click(function(event){
        // For IE:
		if ($.browser.msie) event.returnValue = false;
		
		event.preventDefault();

        formulario = $(this).parent().closest('form').attr('id');
        calculo    = $('#'+formulario).find('input[name=calculo]').val();
        modal      = $('#'+formulario).parent().closest('.modal').attr('id');
        console.log(modal);
        console.log(formulario);
        if(calculo === '8'){
            $('#'+modal).find('.area51-text').html('Aguarde, estou enviando os dados. :)');
            $('#'+modal).find('.alert').addClass('in').removeClass('out').show();

            //$(this).find('button[type=submit]').button('toogle');
                alvo = 'php/devons-mail.php';
        	$(this).enviaForm(formulario, alvo);
        }else{
        	$('#'+modal).find('.area51-text').html('Digite o valor de 5 + 3');
            $('#'+modal).find('.alert').addClass('in').removeClass('out').show();
        }
    });
    $('.area51 .close').click(function(){
        $('.area51 .alert').hide();
    });
    $('.modal-header .close').click(function(){
        $('.area51 .alert').hide();

        
    });

})(jQuery);
  </script>