	<style>
		.modal-header {
			padding: 9px 15px;
			border-bottom: none !important;
		}
	</style>

	 <!-- Modal -->
	 <section id="post-form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-header">
	        <div class="dialog-area"><div class="alert" style="display:none"><a class="close" href="#">&times;</a><span class="dialog-text"></span></div></div>
	    </div>
	    <form id="formMensagem" class="form-horizontal">
	        <div class="modal-body">

	                <label class="" for="name">Name</label>
	                <input type="text" name="name">

	                <label class="" for="email">Email</label>
	                <input type="text" name="email">

	                <label class="" for="message">Write your message</label>
	                <textarea name="message"  rows="6"></textarea>
					<!-- add this line of honeypot to the security of your form -->
	                <span style="display:none !important;visibility:hidden !important;">
		                <input class="thepostman_honeypot" type="text" name="thepostman_honeypot" value="" size="10" tabindex="-1">
		                <br><small>Please leave this field empty.</small>
	                </span>
	                <!-- // honeypot -->

	        </div>
	        <div class="modal-footer">
	            <button type="submit" class="btn" data-loading-text="Sending...">Send your Message</button>
	        </div>
	    </form>
	  </section>
