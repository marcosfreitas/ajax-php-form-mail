<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>AJAX PHP Form Mail 0.3</title>
	<!--
	this source requires bootstrap 2.3.2 css and js scripts and jQuery package.
	If your site already have this, please don't insert again
	-->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
	<!-- // -->

	<!--
	this source need of this plugin to detect the browser.
	-->
	<script src="../bin/js/jquery-browser/jquery.browser.min.js"></script>
</head>
<body>
	<style>
		*:focus {outline: none !important;}
		* {font-family: 'Open Sans', sans-serif;}
		#demo {
			margin: auto;
			text-align: center;
			width: 50%;
		}
		#demo .call-modal {
			margin-top: 15%;
		}
		#demo .call-modal button {
			padding: 20px;
		}
	</style>

	<section id="demo" class="container">
		<div class="row">
			<div class="call-modal well">
				<button id="call-modal-btn" class="btn btn-large btn-block" type="button">Show Post Form</button>
			</div>
		</div>
	</section>

	<?php 
	/**
	 * File of template form
	 */
	require_once('modal.php'); 
	?>

<script>
(function($){

    $('#call-modal-btn').click(function(){
        $('#post-form').modal();
    });

    /*
     * heyPostman() is a function to delivery to the script php (postman.php) the post form
     */

    $.fn.heyPostman = function heyPostman(post_form_id, thePostman_file_url, subject, addressName, addressMail) {

       	// setting the variables
        
        // TODO: Apply verification of empty parameters
        subject	     = subject;
        addressName  = addressName;
        addressMail  = addressMail;
        // populating array
        //var extra_parameters = [];
        //extra_parameters.push({'subject' : subject, 'addressName' : addressName, 'addressMail' : addressMail});
        
        // Creating fields into form
        if($(post_form_id).find('input[name=subject]').length === 0){
        
        	$(post_form_id + ' .modal-body').append('<input type="hidden" name="subject" value="'+subject+'">');
        
        }
        if($(post_form_id).find('input[name=addressName]').length === 0){
	
	        $(post_form_id + ' .modal-body').append('<input type="hidden" name="addressName" value="'+addressName+'">');
    
        }
        if( $(post_form_id).find('input[name=addressMail]').length === 0){

        	$(post_form_id + ' .modal-body').append('<input type="hidden" name="addressMail" value="'+addressMail+'">');
    	}

        message      = null;
        fields       = $(post_form_id).serialize();

        	// sending via ajax
            $.ajax({

              url           : thePostman_file_url,
              type          : 'GET',
              cache         : false,
              dataType      : 'json',
              //contentType : "application/x-www-form-urlencoded;charset=ISO-8859-1",
              contentType   : "application/json;charset=utf-8",
              data          : fields,

              success: function(json, textStatus, xhr) {

                //volta botao e preloader ao normal
                
                if(json.sys === '007'){

	                //reseta botão
	                $(modal).find('button[type=submit]').button('reset');

	                //mensagem final
	                $(modal).find('.dialog-text').html('Mensagem enviada com sucesso, obrigado! :: Thank you! Your message has sent!');

	                $(modal).find('.alert').addClass('in').removeClass('out').show();
                }else{

                    message = message !== null ? message + '<p>'+json.msg+'</p>' : json.msg ;
                    //aviso geral
                    $(modal).find('.dialog-text').html(message);

                    $(modal).find('.alert').addClass('in').removeClass('out').show();

                    $(modal).find('button[type=submit]').button('reset');

                 }
            },

              error: function(xhr, textStatus, errorThrown) {
                console.log(xhr +' '+ textStatus+' '+ errorThrown);
                    //aviso geral
                    $(modal).find('.dialog-text').html('<strong>Tente novamente. Se persistir o erro entre em contato com o suporte. :: Try Again or contact the support team.</strong>');

                    $(modal).find('.alert').addClass('in').removeClass('out').show();

                    $(modal).find('button[type=submit]').button('reset');
              }

            });
       return this;

    };


    /*---------------*/
    $('#formMensagem button[type=submit]').click(function(event){
        // For IE:
			if ($.browser.msie)
				event.returnValue = false;
		
			event.preventDefault();

			//resenting vars
			post_form_id = honeypot_field_length = honeypot_value = modal = textarea_val = input_val = null;

	        post_form_id = $(this).parent().closest('form').attr('id');
	        post_form_id = '#'+post_form_id;
        	
        	// identify the modal of form
	        modal = $(post_form_id).parent().closest('.modal').attr('id');
	        modal = '#'+modal;
        	
        	// stop this function
        	// verify to empty fields
    		textarea_val = $(post_form_id).find('textarea').val();
    		console
    		if($.trim(textarea_val) === ""){
    			$(modal).find('.dialog-text').html('<strong>Preencha todos os campos.</strong>');
                $(modal).find('.alert').addClass('in').removeClass('out').show();

                return;
    		}

        	empty_name = $(post_form_id+' input[type=text]:not([name=thepostman_honeypot])').eq(0).val();
        	empty_mail = $(post_form_id+' input[type=text]:not([name=thepostman_honeypot])').eq(1).val();

        	if($.trim(empty_name) === "" || $.trim(empty_mail) === "") {
        		$(modal).find('.dialog-text').html('<strong>Preencha todos os campos.</strong>');
                $(modal).find('.alert').addClass('in').removeClass('out').show();
			    return;
        	}


        	// Verify if honeypot field exists, if not, this creat he.
        	honeypot_field_length = $(post_form_id).find('input[name=thepostman_honeypot]').length;
        	
        	if(!honeypot_field_length >= 1){
        	
        		$(post_form_id + ' .modal-body').append('<span style="display:none !important;visibility:hidden !important;"><input class="thepostman_honeypot" type="text" name="thepostman_honeypot" value="" size="10" tabindex="-1"><br><small>Please leave this field empty.</small></span>');
        	
        	}else{
        	
        		//verify the value of the field honeypot
        		honeypot_value = $(post_form_id).find('input[name=thepostman_honeypot]').val();
        		
        		if($.trim(honeypot_value) !== ""){
        			alert('this field form is invalid, is you a bot?');
        		}else{
				       
				            $(modal).find('.dialog-text').html('Aguarde, estou enviando os dados. :: Please wait, your message is being sent...');
				            $(modal).find('.alert').addClass('in').removeClass('out').show();

				            // Changing the button state
				            $(this).button('loading');

				                thePostman_file_url = '../bin/php/thePostman.php';
				                subject				= 'Test Form';
				                addressName			= 'Marcos Freitas';
				                addressMail			= 'marcosvsfreitas[at]gmail[dot]com';

				                // TODO: Apply filter. Find and replace dots, @ by [at] and [dot]com[dot]br
				                // see api recaptcha
				                //addressMail			= addressMail.replace('.com', '[dot]com')

				        	$(this).heyPostman(post_form_id, thePostman_file_url, subject, addressName, addressMail);
				}
			}
    });

    $('.dialog-area .close').click(function(){
        $('.dialog-area .alert').hide();
    });
    $('.modal-header .close').click(function(){
        $('.dialog-area .alert').hide();        
    });

})(jQuery);
</script>



</body>
</html>