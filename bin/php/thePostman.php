<?php
header('Content-type: application/json; charset=utf-8');
header_remove();
session_start();session_cache_expire(0);

if($_SERVER['REQUEST_METHOD'] === 'GET'){
#dados
    foreach ($_GET as $chave => $valor) {
        // criando variáveis de acordo com o name dos campos passados na requisição.
        $$chave = utf8_decode($valor);
    }
    $subject = utf8_encode($subject);
    $ipUser = $_SERVER['REMOTE_ADDR'];
    $respostas = array();

#detectando navegador
    require "browserDetector/lib/clsBrowser.php";
    $oBrowser = new clsBrowser();
    try {
        if($oBrowser->Detect()->isDetected()){
            $navegadorUser = $oBrowser->getBrowser().' '.$oBrowser->getVersion();
        }else{
            $navegadorUser = "Não Detectado";
        }
    }catch(Exception $ex){
      echo $ex->getMessage();
    }


try{

#validações
    if(!empty($thepostman_honeypot)){
       throw new Exception('Requisição do formulário inválida');
    }
    if(empty($name)){
       throw new Exception('Faltou o nome', 1);
    }
    if(empty($message)){
       throw new Exception('Faltou a mensagem', 1);
    }
    if(empty($addressName) || empty($addressMail) || empty($subject)){
       throw new Exception('Estamos com problemas técnicos na comunicação com o anunciante. Desculpe, tente mais tarde.', 1);
    }

#validando o e-mail usuário
    $regex = "/^[^0-9][a-zA-Z0-9_-]+([.][a-zA-Z0-9_-]+)*[@][a-zA-Z0-9_-]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/";
    if(empty($email) || !preg_match($regex, $email)){
       throw new Exception('Email incorreto', 1);
    }

#validando o e-mail destinatario
    $addressMail = str_replace('[at]', '@', $addressMail);
    $addressMail = str_replace('[dot]', '.', $addressMail);
    $regex = "/^[^0-9][a-zA-Z0-9_-]+([.][a-zA-Z0-9_-]+)*[@][a-zA-Z0-9_-]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/";
    if(empty($addressMail) || !preg_match($regex, $addressMail)){
       throw new Exception('O e-mail do anunciante está incorreto', 1);
    }

}catch (Exception $e) {
   $respostas['msg'] = $e->getMessage(); //Boring error messages from anything else!
   $respostas['sys'] ='006';
   echo(json_encode($respostas));
   exit();
}

#data de envio
function currentDate(){
    ini_set('date.timezone','America/SAO_PAULO');
    return date("Y-m-d H:i:s");
}
$corpo =(
    "<br/><b>".$subject." de ".$name."</b><br />
     <!--<b><span style=\"color:red\">ATENÇÃO!</span> Não clique em responder e-mail. Verifique o e-mail da pessoa que realizou o contato e responda.</b> -->
     <br/>
     <br/><b>E-mail: </b>".$email."
     <br/><b>Mensagem: </b>".$message."
     <br/>
     <hr/>
     <br/><b>IP: </b>".$ipUser."   <b>Quando? </b>".currentDate().
    "<br/><b>Navegador: </b>".$navegadorUser
);

          #envio do e-mail acontece aqui
            // Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
        require("phpmailer/class.phpmailer.php");
try{
            // Inicia a classe PHPMailer
            $mail = new PHPMailer(true);

            require_once('configuration.php');

            // Define os destinatário(s)
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->AddAddress($addressMail, $addressName); // e-mail destinatário

            // Define os dados técnicos da Mensagem
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)

            // Define a mensagem (Texto e Assunto)
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

            $mail->Subject = $subject." de ".$name; // Assunto da mensagem
            $mail->Body = $corpo;

            // Envia o e-mail
            $enviado = $mail->Send();


            #fim dos código para envio
            // Limpa os destinatÃ¡rios e os anexos
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();
             if ($enviado){

            // Exibe uma mensagem de resultado
               // unlink($caminho.$nameArquivo);//deletando arquivo que foi enviado
                 ob_clean();
                 unset($respostas['msg']);
                 $respostas['sys'] ='007';
                 echo(json_encode($respostas));
             }
             // se retirar o try/catch habilitar este: echo $mail->ErrorInfo;
}catch (phpmailerException $e) {
   //echo $e->errorMessage(); //Pretty error messages from PHPMailer
   //Pretty error messages from PHPMailer
   //echo "Oops, tente de novo.";
   $respostas['msg'] = $e->errorMessage();
   $respostas['sys'] ='006';
   echo(json_encode($respostas));

} catch (Exception $e) {
   $respostas['msg'] = $e->getMessage(); //Boring error messages from anything else!
   $respostas['sys'] ='006';
   echo(json_encode($respostas));
}

}else{
   $respostas['msg'] = "Requisição do formulário inválida";
   $respostas['sys'] = '006';
   echo(json_encode($respostas));

};
?>

