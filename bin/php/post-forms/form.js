var enviaMensagem = {

    Initialize: function(private) {
        //elementos do contato
        private.$formValido = jQuery('#hidden').val();
        private.$nomeUser = jQuery('#nome').val();
        private.$emailUser = jQuery('#email').val();
        private.$telefoneUser = jQuery('#telefone').val();
        private.$msgUser = jQuery('#mensagem').val(); 
    },
    Private:{
        //variáveis privadas do formulário
        $formValido:null,
        $nomeUser:null,
        $emailUser:null,
        $telefoneUser:null,
        $msgUser:null,

        //função privada que vai enviar a mensagem
        privEnviaMSG: function(){
        
            jQuery.ajax({
              url: '_php/_contato/contatoSubmit.php',
              type: 'GET',
              data: {hidden: this.$formValido, nome:this.$nomeUser, email:this.$emailUser, mensagem:this.$msgUser, telefone:this.$telefoneUser},

              complete: function(xhr, textStatus) {
               console.log(xhr);
              },

              success: function(data, textStatus, xhr) {
                jQuery('#statusContato').removeClass('erro');
                jQuery('#statusContato').addClass('aviso');
                res = 'Mensagem enviada!';
                  
                     //depois de um pouco mais de 10 segundos reseta-se o formulário.
                     setTimeout(function(){
                        $('#formContato').each(function(){
                                this.reset();
                                jQuery('#captcha').attr({'src':'_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random()});
                                jQuery('#captchaInp').focus();
                        });
                    }, 10200);

                HideLoading();
              },

              error: function(xhr, textStatus, errorThrown) {
                jQuery('#statusContato').removeClass('aviso');jQuery('#statusContato').addClass('erro');
                jQuery('#captcha').attr({'src':'_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random()});
                jQuery('#captchaInp').focus();
              }
            });
            
        }
    },

    Public:{
        getEnviaMSG: function(private){
            private.privEnviaMSG();
        }
    }
}


//função para instanciação do objeto
function themakeclass(){
    $.makeClass(enviaMensagem);
}

//função de validação do captcha digitado
function validaCaptcha(){
    //ao enviar os dados do formulário
    ShowLoading();
    //valor do captcha digitado
    $nameCaptcha = jQuery('#captchaInp').val();

    jQuery.ajax({
          url: '_php/_contato/validaCaptcha.php',
          type: 'GET',
          data: {captcha:$nameCaptcha},
          complete: function(xhr, textStatus) {
            console.log(xhr);
          },
          success: function(data, textStatus, xhr) {
            console.log(data);
             res = "Enviando...";
             jQuery('#statusContato').removeClass('erro');
             jQuery('#statusContato').addClass('aviso');
             //chamada classe de envio
             themakeclass();
    
          },
          error: function(xhr, textStatus, errorThrown) {
            console.log(xhr);
            res = 'C�digo Inv�lido';
            jQuery('#statusContato').removeClass('aviso');
            jQuery('#statusContato').addClass('erro');
            jQuery('#captcha').attr({'src':'_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random()});
            jQuery('#captchaInp').focus();
          }
    });

}
