
<?
header('Content-Type: text/html; charset=iso-8859-1');
error_reporting(false);
?>
<!--  formul�rio de contato v.1.0 por Marcos Freitas marcosvsfreitas@gmail.com -->
<script type="text/javascript">
//ações sobre o loading
function ShowLoading(){
    //mostrando loading
    jQuery('#capaLoading-fc').show();
    jQuery('#loading-fc').show();
    jQuery('#statusContato').html('Aguarde...');
    
};

function HideLoading($res){
    //mostra mensagem de resposta
    jQuery('#statusContato').html($res);
    
    setTimeout(function(){
         //escondendo loading
        jQuery('#capaLoading-fc').hide();
        jQuery('#loading-fc').hide();
    }, 5000);


};

function enviaMSG(){
        //elementos do contato
        $formValido = jQuery('#hidden').val();
        $nomeUser = jQuery('#nome').val();
        $emailUser = jQuery('#email').val();
        $telefoneUser = jQuery('#telefone').val();
        $msgUser = jQuery('#mensagem').val(); 


        
            jQuery.ajax({
              url: '_php/_contato/contatoSubmit.php',
              type: 'GET',
              data: {hidden: $formValido, nome:$nomeUser, email:$emailUser, mensagem:$msgUser, telefone:$telefoneUser},
              
              success: function(data, textStatus, xhr) {
                res = data;
                //console.warn('data enviamsg: ', data);
                if(res == '007'){
                  jQuery('#statusContato').removeClass('erro');
                  jQuery('#statusContato').addClass('aviso');
                  res = 'Mensagem enviada!';
                  jQuery('#statusContato').html(res);
                     setTimeout(function(){
                        $('#formContato').each(function(){
                                jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                                jQuery('#captchaInp').focus();
                        });
                    }, 5000);
                    
                }else{ 
                 jQuery('#statusContato').removeClass('aviso');jQuery('#statusContato').addClass('erro');
                 jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                 jQuery('#captchaInp').focus();
                }

               HideLoading(res);
              },
              error: function(xhr, textStatus, errorThrown) {
                res = 'Tente novamente...';
                jQuery('#statusContato').removeClass('aviso');jQuery('#statusContato').addClass('erro');
                jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                jQuery('#captchaInp').focus();
                HideLoading(res);
              }
            });
};         

//fun��o de valida��o do captcha digitado
function validaCaptcha(){
    //ao enviar os dados do formulário
    ShowLoading();
    //mostra mensagem de resposta
    jQuery('#statusContato').html('Enviando...');
    //valor do captcha digitado
    $nameCaptcha = jQuery('#captchaInp').val();

    jQuery.ajax({
          url: '_php/_contato/validaCaptcha.php',
          type: 'GET',
          data: {captcha:$nameCaptcha},
          success: function(data, textStatus, xhr) {
                res = data;
                console.warn('res '+res);
            if (res == 'E'){
                res = 'C�digo Inv�lido';
                jQuery('#statusContato').removeClass('aviso');
                jQuery('#statusContato').addClass('erro');
                jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                jQuery('#captchaInp').focus();
                //mostra mensagem de resposta
                jQuery('#statusContato').html(res);
                HideLoading(res);
            }else if(res == 'S'){
                res = "Enviando...";
                //console.log(res);
                jQuery('#statusContato').removeClass('erro');
                jQuery('#statusContato').addClass('aviso');
                 //mostra mensagem de resposta
                jQuery('#statusContato').html(res);
                enviaMSG();
            }else if(res == 'F'){
                res = 'Digite o c�digo.';
                jQuery('#statusContato').removeClass('aviso');
                jQuery('#statusContato').addClass('erro');
                jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                jQuery('#captchaInp').focus();
               //mostra mensagem de resposta
                jQuery('#statusContato').html(res);
                HideLoading(res);
            }

          },
          error: function(xhr, textStatus, errorThrown) {
           console.log(xhr);
            res = 'Tente Novamente...';
            jQuery('#statusContato').removeClass('aviso');
            jQuery('#statusContato').addClass('erro');
            jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
            jQuery('#captchaInp').focus();
          }
    });

};


</script>

<style type="text/css">
    #corpo-fc{
        width: 550px;
        height: auto;
        text-align: center;
        position: relative;
        z-index: 2;
    }
    #corpo-central-fc{
        width: 550px; height: auto;
        text-align: center;
    }
    #corpo-fc #conteudo-central-fc{
        width: 100%; height: auto;
    }
   
    #fale{
        width:100%;
        height: auto;
        margin: 0 auto;
    }
    #fale #alinha-faleA{
        text-align:center; margin: 0 auto; width: 100%; height: auto;
    }
    #fale #faleA{
        width: auto; height: auto; margin: 0 auto;
       
      
    }
    .styleInput{
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    border: 4px solid #eee;
    background: #fff;
    }
    #faleA #inp input{
    width: auto; height: 21px; border: 4px solid #ccc;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    padding: 2px 4px;
    margin: 5px 0 0 0;
    background: #fff;
    }
    textarea:focus, textarea:hover, input:focus, input:hover{
    -webkit-box-shadow: inset 0px 2px 5px 0px #666;
    box-shadow: inset 0px 2px 5px 0px #666;
    border: 4px solid #CCC;
    }
    #capt{
        width:345px; height: auto;
        margin-top: 5px; padding: 5px;
        min-height: 102px; float:right;
    }
    #capt-left{
        width: 181px; height: 105px;
        float: left;
    }
    #capt-left a{color: #3399ff}
    #capt-right{
        width: auto; height: auto; margin-top: 30px;
        float: right; color:#888;
        max-width: 153px; overflow: hidden;
    }
    #capt-right input{
        width: 124px; height: 18px; border: 1px solid #ccc; padding: 2px 5px;display: block; margin: 0 auto;
    }
    #faleA #inp{
        width: 175px; height: auto;float:left; padding: 5px;
    }
    #msg{
        width: 99%; height: 148px; float: left; padding: 10px 0; text-align:left;
    }
    #msg textarea{
        width: 98%; height: 88%; padding: 5px;  border: 4px solid #CCC;
    }
    .btEnviar{
     background: #008200;
     opacity:0.7; filter:alpha(opacity = 70);
     width:auto; height: auto;
     position: relative;right: 9px;top: -40px;float:right;z-index: 20;
     padding: 3px 4px;margin-right: 4px;
     cursor:pointer;
     color: #fff;
     z-index: 1;
    }
    .btEnviar:hover{opacity: 1;}
</style>



<div id="corpo-fc">
    <div id="corpo-central-fc">
        <div id="conteudo-central-fc">
            
            <form id="formContato" action="_php/_contato/contatoSubmit.php" method="GET" class="morfar">
                <div id="fale">
                    <div id="alinha-faleA">
                    <div id="faleA">
                        <div id="inp">
                         <input type="hidden" id="hidden" name="hidden" value="<? /*session_start();*/ $_SESSION['hidden'] = md5('ons26112012'); echo $_SESSION['hidden'];?>"/>
                         <input tabindex='1' type="text" name="nome" class="font14px fontDroid" id="nome" value="Seu nome"/>   
                         <input tabindex='2' type="text" name="email" class="font14px fontDroid" id="email" value="Seu e-mail"/>   
                         <input tabindex='3' type="text" name="telefone"  class="font14px fontDroid" id="telefone" value="Seu telefone"/>   
                        </div>
                        <div id="capt" class="styleInput">
                            <div id="capt-left">
                                <img src="_php/cool-php-captcha-0.3.1/captcha.php" id="captcha" /><br/>
                                                            <!-- CHANGE TEXT LINK -->
                            <a class="txShad" href="javascript:void(0)" onclick="
                                jQuery('#captcha').attr('src','_php/cool-php-captcha-0.3.1/captcha.php?'+Math.random());
                                jQuery('#captchaInp').focus();"
                                id="change-image">Mudar a imagem.</a>
                                <?//session_start(); echo("<script>console.log('cap: ','".$_SESSION['captcha']."');</script>");?>
                            </div> 
                            <div id="capt-right" >
                                <p class="font14px fontDroid" style="height:21px; max-height: 21px;overflow: hidden;margin-bottom: 5px;" id="statusContato">Digite o c�digo ao lado</p>
                                <input tabindex='5' id="captchaInp" class="font14px fontDroid" type="text" value=""/>   
                            </div> 
                            <div class="limpar"></div>
                        </div>
                    <div class="limpar"></div>    
                    <div id="msg">
                        <div style="text-align: center;margin: 0 auto; width: 100%; height: 100%;">  
                            <textarea tabindex='4' class="styleInput font14px fontDroid" id="mensagem" name="mensagem">Sua mensagem</textarea>
                            <div onclick="validaCaptcha();" id='btEnviar' class="btEnviar font20px fontTitilliumBold">Enviar</div>
                        </div>
                    <div class="limpar"></div>
                    </div>
                  </div>
                    <div class="limpar"></div>
                    </div>
                    <div class="limpar"></div>
                </div>
            </form>
        </div>
    </div>

</div>


<script type="text/javascript">

/** VALIDAÇÃO DO FORMULÁRIO **/
//validando formulário de interesse

jQuery('#formContato input,#formContato textarea').focus(function(){
   $value = this.value;
   $idInput = this.id;
   if($value.match(/(Seu nome|Seu e-mail|Seu telefone|Sua mensagem)/gi) || $idInput == 'captchaInp')
   this.value = '';
   
});    
    //validando o formul�rio para os valores deixados iguais.
jQuery('#formContato input,#formContato textarea').blur(function(){
    //vericando se o valor do campo mudou
    $atualValue = jQuery('#'+$idInput).val();
    $valid = false;
    
        //validando o valor pego no no blur, para verificar se � um valor padr�o
        switch($atualValue){
        //o atualValue no envento blur() sempre vai ser vazio.
        /*
        quando o foco sai do campo input
        e o usu�rio n�o tiver digitado nada
        o valor atual que � pego � VAZIO
        por isso deve-se tratar com a condi��o abaixo.

        trantando-se de campo VAZIO, deve-se voltar ao valor de antes
        que � pego na vari�vel $value, verificando os padr�es novamente
        trantando a validade ($valid) como FALSE
        */
            case '':
                this.value = $value;
                $valid = false;

                if($idInput != 'captchaInp'){
                    //validando os padr�es novamente
                    switch(this.value){
                        case 'Seu nome':
                            $valid = false;
                            break;
                        case 'Seu e-mail':
                            $valid = false;
                            break;
                        case 'Seu telefone':
                            $valid = false;
                            break;
                        case '(___) ____-____':
                            $valid = false;
                            break;
                        case 'Sua mensagem':
                            $valid = false;
                            break;
                        default:
                            jQuery('#'+$idInput).val($value);
                            $valid = true;
                            break;

                    }
                }

                break;
            default:
                $valid = true;
                break;
        }
    
    /*setando o valor do input como o valor pego no focus
    se o valor for padr�o, como na lista do switch, ele vai dar
    erro, caso contr�rio, no default ser� apontado como v�lido.*/
   
    if($valid == false){
        this.value = $value;
       jQuery('#'+this.id).removeAttr('style').css('border','4px solid #f24b3f');
    }else{
        jQuery('#'+this.id).removeAttr('style').css('border','4px solid #CCC');
    }
});

function verificaNumero(e){
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
        }
};

jQuery('#telefone').focus(function(){
        //máscara para o telefone
        if($value == 'Seu telefone'){
           jQuery('#telefone').mask("(999) 9999-9999");
        }
        jQuery('#telefone').keypress(verificaNumero);
        
});

jQuery('#telefone').blur(function(){
           if($('#telefone').val() == 'Seu telefone' || this.value == '(___) ____-____'){
                setTimeout(function(){$('#telefone').val('Seu telefone');}, 1000)
           } /*else {
            console.warn ('tratar exce��o');
            console.warn('valor do input: '+this.value)
            console.warn('tamanho do input: '+$('#telefone').val().length);
           }*/
});
</script>