<?PHP 
/**
 * Easy Browser Detection
 *
 * @author      Muneeb <m4munib@hotmail.com>
 * @copyright   Muneeb <m4munib@hotmail.com>
 * @twitter     http://twitter.com/#!/muhammadmunib
 */
include"lib/clsBrowser.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Easy Browser Detection</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" media="screen" href="assets/style.css" /> 
<link type="text/css" rel="stylesheet" media="all" href="assets/highlight.css" />
<meta name="author"  content="Muneeb" />
<meta name="publisher"  content="Muneeb" />
<meta name="copyright"  content="2011 Muneeb" />
<script type="text/javascript" src="assets/jquery.min.js"></script>
<script type="text/javascript" src="assets/highlight.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
            $('pre.code').highlight({source:1, zebra:1, indent:'space', list:'ol'});
    });
</script>
</head>

<body>
    <div class="wrapper">         
        <h1>Easy Browser Detection</h1> 
        <p align="center">By: <a href='http://codecanyon.net/user/amzee/portfolio?ref=amzee'>Muneeb</a></p>
        
        <div class="block">
            <h2>Features</h2>
            <ul class="features">
                <li>Detects all the modern browsers</li>
                <li><strong>Supports</strong> - 'ie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape','konqueror', 'gecko' and 'flock'</li>
                <li>Detect Version number.</li>
                <li>Lightweight.</li>
                <li>Easy to Use.</li>
                <li>Many Examples included.</li>
                <li>IE6 Notification - Use it the way you like</li>
                <li>Uses standard PHP - No additional extension support needed.</li>
                <li>Detection - Minimum 2 Line of Code.</li>
                <li>Very Flexible - You can specify your custom detection routine.</li>
                <li>Extendible Browsers List.</li>
                <li>Ease of Integration.</li>
                <li>Fully OOP based - PHP 5</li>
                <li>Ease to Test - Builtin Support for Testing.</li>
            </ul>
        </div>
        
        
        <div class="block">
            <?PHP include"examples/generic.example.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/detect.ie.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/ie6.notification.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/detect.chrome.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/detect.firefox.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/detect.safari.php"; ?>
        </div>
   
        <div class="block">
            <?PHP include"examples/extend.browsers.list.php"; ?>
        </div>
        
        <div class="block">
            <?PHP include"examples/custom.browser.detection.class.php"; ?>
        </div>
        
       
    </div>
</body>
</html>
