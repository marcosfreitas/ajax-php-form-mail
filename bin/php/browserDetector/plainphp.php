<?PHP
/**
 * Easy Browser Detection
 *
 * @author      Muneeb <m4munib@hotmail.com>
 * @copyright   Muneeb <m4munib@hotmail.com>
 * @twitter     http://twitter.com/#!/muhammadmunib
 */
include"lib/clsBrowser.php";
$oBrowser = new clsBrowser();
echo "<h2>Browser Detection</h2>";
try
{
    if($oBrowser->Detect()->isDetected())
    {
        echo "Browser - " . $oBrowser->getBrowser();
        echo "<br />Version - " . $oBrowser->getVersion();
    }
    else
    {
        echo "Not Detected";    
    }
}catch(Exception $ex){echo $ex->getMessage();}
?>